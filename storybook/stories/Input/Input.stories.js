import VinInput from '@VinComponents/input';
import {colors} from '@VinUtils/colors';
import {storiesOf} from '@storybook/react-native';
import React from 'react';

import CenterView from '../CenterView';

storiesOf('Input', module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('with text', () => (
    <VinInput
      secureTextEntry
      errorText="This is an error"
      keyboardType="default"
      placeholder="passeword"
      placeholderTextColor={colors.text.tertiaryGrey}
      backgroundColor={colors.app.white}
      borderColor={colors.app.primary}
    />
  ));
