import {storiesOf} from '@storybook/react-native';
import React from 'react';

import VinHearder from '@VinSrc/components/header';
import CenterView from '../CenterView';

storiesOf('Header', module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('with text', () => <VinHearder searchBottom logo transparent />);
