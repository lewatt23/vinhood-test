import {VinCard} from '@VinComponents/card';

import {storiesOf} from '@storybook/react-native';
import React from 'react';

import {VinListItemSimple} from '@VinSrc/components/card/listItem';
import CenterView from '../CenterView';

storiesOf('Card', module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('card', () => (
    <VinCard
      onPressCard={() => {}}
      post={{
        page: {
          name: 'Mfouou medjo stanly',
          profile: {
            url: 'https://www.fillmurray.com/640/360',
          },
        },
        createdAt: new Date(),
        title: 'Ric Shanchze',
        content:
          'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.',
      }}
    />
  ))
  .add('list card', () => (
    <VinListItemSimple
      width={40}
      height={40}
      uri={{uri: ''}}
      title="item.title"
      onPress={() => {}}
      style={{height: 90, marginTop: 10, paddingVertical: 12}}
    />
  ));
