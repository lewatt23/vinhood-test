import appJson from '../../app.json';

const config =
  process.env.NODE_ENV === 'production'
    ? appJson.scheme.production
    : appJson.scheme.development;

export const {APP_BASE_URI} = config;
