import {configureInterceptors} from '@VinSrc/services/interceptor';
import {ReactElement, useEffect} from 'react';
import {Store} from 'redux';
import NetInfo from '@react-native-community/netinfo';
import {ToastService} from '@VinSrc/services';

interface InterceptorInterface {
  children: ReactElement;
  store: Store;
}

// This provider is used to provide the interceptos with default auth values

function InterceptorProvider(props: InterceptorInterface) {
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      if (!state.isInternetReachable) {
        ToastService.showToast({
          message: 'your not connected to the internet ',
          type: 'warning',
          duration: 3000,
        });
      }
    });

    return () => {
      unsubscribe();
    };
  }, []);

  useEffect(() => {
    configureInterceptors(props.store);
  });

  return props.children;
}
export default InterceptorProvider;
