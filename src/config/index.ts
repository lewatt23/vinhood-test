import {configStore} from '@VinSrc/store/configStore';

const {store, persistor} = configStore();

export {store, persistor};
