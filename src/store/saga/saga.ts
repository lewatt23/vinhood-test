import {ILocation} from '@VinSrc/typings/apiTypes';
import {getCharacterIDFromUrl} from '@VinSrc/utils/location';
import {store} from '@VinSrc/config';
import axios, {AxiosResponse, Method} from 'axios';
import {call, put, takeLatest} from 'redux-saga/effects';
import {selectLocations} from '../reducers/locations';
import {
  CharacterGetAllActionType,
  CharactersGetByLocationActionType,
  LocationsGetAllActionType,
} from '../types';
import * as t from '../actions';

function apicaller(method: Method, url: string, data?: any) {
  return axios({
    method,
    url,
    data,
  });
}

function* CharactersGetAll(action: CharacterGetAllActionType) {
  try {
    if (!action.url) return;

    const result: AxiosResponse = yield call(apicaller, 'get', action.url);

    yield put({
      type: t.CHARACTERS_GET_ALL_SUCCESS,
      payload: {
        ...result.data,
      },
    });
  } catch (error: unknown) {
    console.error('saga fail: ', error);
  }
}
function* CharactersFilter(action: CharacterGetAllActionType) {
  try {
    if (!action.url) return;

    yield put({
      type: t.CHARACTERS_SET_LOADING,
    });

    const result: AxiosResponse = yield call(apicaller, 'get', action.url);

    yield put({
      type: t.CHARACTERS_FILTER_SUCCESS,
      payload: {
        ...result.data,
      },
    });
  } catch (error: unknown) {
    console.error('saga fail: ', error);
  }
}

function* CharactersGetByLocation(action: CharactersGetByLocationActionType) {
  try {
    if (!action.id) return;

    yield put({
      type: t.CHARACTERS_SET_LOADING,
    });

    const location: ILocation[] | [] = selectLocations(store.getState()).filter(
      (loc: ILocation) => {
        if (loc.id === action.id) {
          return loc.residents;
        }

        return null;
      },
    );

    if (!location.length) {
      return;
    }

    const {residents} = location[0];

    const residentsIds = getCharacterIDFromUrl(residents);

    const url = `https://rickandmortyapi.com/api/character/${residentsIds.toString()}`;

    const result: AxiosResponse = yield call(apicaller, 'get', url);
    // console.log(result.data);
    yield put({
      type: t.CHARACTERS_GET_BY_LOCATION_SUCCESS,
      payload: {
        results: result.data.length ? result.data : [result.data],
      },
    });
  } catch (error: unknown) {
    console.error('saga fail: ', error);
  }
}

function* CharactersRefresh() {
  try {
    yield put({
      type: t.CHARACTERS_SET_LOADING,
    });

    const result: AxiosResponse = yield call(
      apicaller,
      'get',
      'https://rickandmortyapi.com/api/character',
    );
    yield put({
      type: t.CHARACTERS_REFRESH_SUCCESS,
      payload: {
        ...result.data,
      },
    });
  } catch (error: unknown) {
    console.error('saga fail: ', error);
  }
}

// Locations
function* LocationsGetAll(action: LocationsGetAllActionType) {
  try {
    if (!action.url) return;

    const result: AxiosResponse = yield call(apicaller, 'get', action.url);
    yield put({
      type: t.LOCATIONS_GET_ALL_SUCCESS,
      payload: {
        ...result.data,
      },
    });
  } catch (error: unknown) {
    console.error('saga fail: ', error);
  }
}

function* LocationsRefresh() {
  try {
    yield put({
      type: t.LOCATIONS_SET_LOADING,
    });

    const result: AxiosResponse = yield call(
      apicaller,
      'get',
      'https://rickandmortyapi.com/api/location',
    );

    yield put({
      type: t.LOCATIONS_REFRESH_SUCCESS,
      payload: {
        ...result.data,
      },
    });
  } catch (error: unknown) {
    console.error('saga fail: ', error);
  }
}

export function* watcherCharactersGetAll() {
  yield takeLatest(t.CHARACTERS_GET_ALL, CharactersGetAll);
}
export function* watcherCharactersFilter() {
  yield takeLatest(t.CHARACTERS_FILTER, CharactersFilter);
}

export function* watcherCharactersGetByLocationAction() {
  yield takeLatest(t.CHARACTERS_GET_BY_LOCATION, CharactersGetByLocation);
}

export function* watcherCharactersRefresh() {
  yield takeLatest(t.CHARACTERS_REFRESH, CharactersRefresh);
}

export function* watcherLocationsGetAll() {
  yield takeLatest(t.LOCATIONS_GET_ALL, LocationsGetAll);
}

export function* watcherLocationsRefresh() {
  yield takeLatest(t.LOCATIONS_REFRESH, LocationsRefresh);
}
