import {fork} from 'redux-saga/effects';
import {
  watcherCharactersGetAll,
  watcherCharactersRefresh,
  watcherLocationsGetAll,
  watcherCharactersGetByLocationAction,
  watcherLocationsRefresh,
  watcherCharactersFilter,
} from './saga';

export default function* rootSaga() {
  yield fork(watcherCharactersGetAll);
  yield fork(watcherCharactersRefresh);
  yield fork(watcherLocationsGetAll);
  yield fork(watcherLocationsRefresh);
  yield fork(watcherCharactersGetByLocationAction);
  yield fork(watcherCharactersFilter);
}
