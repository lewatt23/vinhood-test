import {persistStore, persistReducer, Persistor} from 'redux-persist';
import {composeWithDevTools} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  createStore,
  combineReducers,
  applyMiddleware,
  Store,
  compose,
} from 'redux';
import {CharacterGetAllActionType} from './types';
import rootSaga from './saga';
import {charactersReducer} from './reducers/characters';
import {locationsReducer} from './reducers/locations';

const composeEnhancers = compose;

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const rootReducer = combineReducers({
  characters: charactersReducer,
  locations: locationsReducer,
});

export default rootReducer;
// typing for reducer
export type RootState = ReturnType<typeof rootReducer>;

export interface SelectorFn<V, A = undefined> {
  (state: RootState, args?: A): V;
}

export type RootActionTypes = CharacterGetAllActionType;

// persist
const persistedReducer = persistReducer(persistConfig, rootReducer);

// saga
const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

if (__DEV__) {
  const createDebugger = require('redux-flipper').default;
  middlewares.push(createDebugger());
}

// configure store with types
export const configStore = function configureStore(): {
  store: Store<RootState>;
  persistor: Persistor;
} {
  const store = createStore(
    persistedReducer,
    {},
    composeEnhancers(composeWithDevTools(applyMiddleware(...middlewares))),
  );

  const persistor = persistStore(store);
  persistor.purge();

  sagaMiddleware.run(rootSaga);

  return {persistor, store};
};
