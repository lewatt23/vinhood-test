import {
  LocationsRefreshReducerType,
  LocationsGetAllReducerType,
  LocationsLoadingReducerType,
  LocationsState,
} from '../types';
import * as t from '../actions';

const locationInitalState: LocationsState = {
  info: {
    count: 0,
    pages: 1,
    next: 'https://rickandmortyapi.com/api/location',
  },
  loading: false,
  results: [],
};

export const selectLocations: any = (state: any) => state.locations.results;
export const selectLoading: any = (state: any) => state.locations.loading;
export const selectLocationsPaginationInfo: any = (state: any) =>
  state.locations.info;

const locationsReducer = (
  state = locationInitalState,
  action:
    | LocationsGetAllReducerType
    | LocationsRefreshReducerType
    | LocationsLoadingReducerType,
) => {
  switch (action.type) {
    case t.LOCATIONS_GET_ALL_SUCCESS:
      if (!action.payload) return state;

      state = {
        info: action.payload.info,
        results: [...state.results, ...action.payload.results],
      };
      return state;
    case t.LOCATIONS_REFRESH_SUCCESS:
      if (!action.payload) return state;

      state = {
        info: action.payload.info,
        results: action.payload.results,
        loading: false,
      };
      return state;
    case t.LOCATIONS_SET_LOADING:
      state = {
        ...state,
        loading: true,
      };
      return state;

    default:
      return state;
  }
};

export {locationsReducer};
