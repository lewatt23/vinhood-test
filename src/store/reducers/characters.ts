import {
  CharactersFilterReducerType,
  CharactersGetAllReducerType,
  CharactersGetByLocationReducerType,
  CharactersRefreshReducerType,
  CharactersSetReducerType,
  CharactersState,
} from '../types';
import * as t from '../actions';

const charatersInitalState: CharactersState = {
  info: {
    count: 0,
    pages: 1,
    next: 'https://rickandmortyapi.com/api/character',
  },
  loading: false,
  results: [],
};

export const selectCharacters: any = (state: any) => state.characters.results;
export const selectCharactersLoading: any = (state: any) =>
  state.characters.loading;
export const selectCharactersPaginationInfo: any = (state: any) =>
  state.characters.info;

const charactersReducer = (
  state = charatersInitalState,
  action:
    | CharactersGetAllReducerType
    | CharactersRefreshReducerType
    | CharactersGetByLocationReducerType
    | CharactersSetReducerType
    | CharactersFilterReducerType,
) => {
  switch (action.type) {
    case t.CHARACTERS_GET_ALL_SUCCESS:
      if (!action.payload) return state;
      state = {
        info: action.payload.info,
        results: [...state.results, ...action.payload.results],
      };
      return state;
    case t.CHARACTERS_REFRESH_SUCCESS:
      if (!action.payload) return state;
      state = {...action.payload, loading: false};
      return state;
    case t.CHARACTERS_FILTER_SUCCESS:
      if (!action.payload) return state;
      state = {...action.payload, loading: false};
      return state;
    case t.CHARACTERS_GET_BY_LOCATION_SUCCESS:
      if (!action.payload) return state;

      state = {
        info: {
          count: action.payload.results.length,
          pages: 1,
          next: 'https://rickandmortyapi.com/api/character',
        },
        results: action.payload.results,
        loading: false,
      };

      return state;

    case t.CHARACTERS_SET_LOADING:
      state = {
        ...state,
        loading: true,
      };
      return state;

    default:
      return state;
  }
};

export {charactersReducer};
