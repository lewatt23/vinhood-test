import {ICharacter, ILocation} from '@VinSrc/typings/apiTypes';
import {IPagintionInfo} from '../typings/apiTypes';
import * as t from './actions';

export interface CharactersState {
  info: IPagintionInfo;
  loading?: boolean;
  results: ICharacter[] | [];
}

export interface LocationsState {
  info: IPagintionInfo;
  loading?: boolean;
  results: ILocation[] | [];
}

export interface CharacterGetAllActionType {
  type: typeof t.CHARACTERS_GET_ALL;
  url: string;
}

export interface CharactersGetAllReducerType {
  type: typeof t.CHARACTERS_GET_ALL_SUCCESS;
  payload: CharactersState;
}

export interface CharactersRefreshReducerType {
  type: typeof t.CHARACTERS_REFRESH_SUCCESS;
  payload: CharactersState;
}

// Locations
export interface LocationsGetAllActionType {
  type: typeof t.LOCATIONS_GET_ALL;
  url: string;
}

export interface LocationsGetAllReducerType {
  type: typeof t.LOCATIONS_GET_ALL_SUCCESS;
  payload: LocationsState;
}

export interface CharactersGetByLocationActionType {
  type: typeof t.CHARACTERS_GET_BY_LOCATION;
  id: number;
}

export interface CharactersGetByLocationReducerType {
  type: typeof t.CHARACTERS_GET_BY_LOCATION_SUCCESS;
  payload: CharactersState;
}

export interface LocationsRefreshReducerType {
  type: typeof t.LOCATIONS_REFRESH_SUCCESS;
  payload: LocationsState;
}
export interface LocationsLoadingReducerType {
  type: typeof t.LOCATIONS_SET_LOADING;
}
export interface CharactersSetReducerType {
  type: typeof t.CHARACTERS_SET_LOADING;
}
export interface CharactersFilterReducerType {
  type: typeof t.CHARACTERS_FILTER_SUCCESS;
  payload: CharactersState;
}
