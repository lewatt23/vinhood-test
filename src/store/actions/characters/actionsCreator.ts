import * as t from './actionsTypes';

export const CharactersGetAllAction = (url: string) => ({
  type: t.CHARACTERS_GET_ALL,
  url,
});

export const CharactersGetByLocationAction = (id: number) => ({
  type: t.CHARACTERS_GET_BY_LOCATION,
  id,
});

export const CharactersRefreshAction = () => ({
  type: t.CHARACTERS_REFRESH,
});
export const CharactersFilterAction = (url: string) => ({
  type: t.CHARACTERS_FILTER,
  url,
});
