import * as t from './actionsTypes';

export const LocationsGetAllAction = (url: string) => ({
  type: t.LOCATIONS_GET_ALL,
  url,
});

export const LocationsRefreshAction = () => ({
  type: t.LOCATIONS_REFRESH,
});
