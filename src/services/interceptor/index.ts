import {APP_BASE_URI} from '@VinSrc/config/constants';
import {ToastService} from '@VinSrc/services';
import {RootState} from '@VinSrc/store/configStore';
import axios, {AxiosError, AxiosRequestConfig, AxiosPromise} from 'axios';
import {Store} from 'redux';

axios.defaults.baseURL = APP_BASE_URI;
// not showing alert for these url
const urlsException = ['/apps/app-instance', '/auth/refresh-access-token'];

export type InterceptorCallback = (
  store: Store,
  api: AxiosRequest,
) => Promise<unknown> | unknown;

export interface ConfigAxios extends AxiosRequestConfig {
  __isRetryRequest?: boolean;
  __override?: boolean;
}

export interface InterceptorError extends AxiosError {
  config: ConfigAxios;
}

export type AxiosRequest = (options: ConfigAxios) => AxiosPromise;

export const requestInterceptor = (store: Store) => {
  const {auth} = store.getState();

  return axios.interceptors.request.use(async (config: ConfigAxios) => {
    if (config.url === '/auth/refresh-access-token' || config.__override) {
      return config;
    }

    config!.headers!.Authorization = `Bearer ${auth?.user?.accessToken}`;

    return config;
  });
};

export const refreshToken: InterceptorCallback = async (store, api) => {
  const token = store.getState().auth;
  try {
    const res = await api({
      url: '/auth/refresh-access-token',
      headers: {
        'X-Auth': `${token.refreshToken}`,
      },
      method: 'GET',
    });

    // store.dispatch();

    return Promise.resolve(res.data.accessToken);
  } catch (authError: unknown) {
    // refreshing has failed, but report the original error, i.e. 401
    return Promise.reject(authError);
  }
};

export const errorInterceptor = (code: number | string) => {
  axios.interceptors.response.use(
    undefined,
    async (error: InterceptorError) => {
      if (code === '*') {
        if (urlsException.includes(error.response?.config.url!)) {
          return Promise.reject(error);
        }

        ToastService.showToast({
          message: error.name,
          description: error.response?.data?.message,
          type: 'danger',
        });
        return Promise.reject(error);
      }

      return Promise.reject(error);
    },
  );
};

export const configureInterceptors = async (store: Store<RootState>) => {
  errorInterceptor('*');
  requestInterceptor(store);
};
