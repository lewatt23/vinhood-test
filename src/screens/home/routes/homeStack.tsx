import React, {FunctionComponent} from 'react';
import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import CharactersScreen from '../scenes/charactersScreen';

const StackNavigator = createStackNavigator<any>();

export const HomeStack: FunctionComponent<HomeStackProps> = () => (
  <StackNavigator.Navigator
    screenOptions={{
      headerShown: false,
    }}
    initialRouteName="HomeTab">
    <StackNavigator.Screen name="HomeTab" component={CharactersScreen} />
  </StackNavigator.Navigator>
);

interface HomeStackProps {
  route: RouteProp<any, any>;

  navigation: StackNavigationProp<any, any>;
}
