/* eslint-disable camelcase */

import {colors} from '@VinSrc/utils';
import {
  CompositeNavigationProp,
  NavigationProp,
  RouteProp,
} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import React, {FunctionComponent, useCallback, useEffect} from 'react';
import {
  ActivityIndicator,
  FlatList,
  ListRenderItem,
  StyleSheet,
  View,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import VinHearder from '@VinSrc/components/header';
import {VinListItemSimple} from '@VinSrc/components/card/listItem';
import {useDispatch, useSelector} from 'react-redux';
import {
  LocationsGetAllAction,
  LocationsRefreshAction,
} from '@VinSrc/store/actions';
import {
  selectLocationsPaginationInfo,
  selectLocations,
  selectLoading,
} from '@VinSrc/store/reducers/locations';

const LocationsScreen: FunctionComponent<CharactersScreenProps> = ({
  navigation,
}) => {
  const dispatch = useDispatch();

  const loc: any = useSelector(selectLocationsPaginationInfo);
  const locations = useSelector(selectLocations);
  const loading = useSelector(selectLoading);

  useEffect(() => {
    dispatch(LocationsGetAllAction(loc?.next));
  }, []);

  const renderItem: ListRenderItem<any> = useCallback(
    ({item}) => (
      <VinListItemSimple
        title={item.name}
        type={item.type}
        dimension={item.dimension}
        number={item.residents.length}
        onPress={async () => {
          await AsyncStorage.setItem('location', JSON.stringify(item.id));
          navigation!.navigate('CharactersStack');
        }}
        style={styles.vin}
      />
    ),
    [navigation],
  );

  const renderFooter = () => {
    if (loc?.next) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color={colors.app.primary} />
        </View>
      );
    }
    return <View />;
  };

  const renderEmpty = () => <View />;

  return (
    <View style={styles.container}>
      <VinHearder logo />

      <FlatList
        style={styles.flatlist}
        initialNumToRender={5}
        keyExtractor={item => String(item.id)}
        data={locations || []}
        renderItem={renderItem}
        ListEmptyComponent={renderEmpty}
        ListFooterComponent={renderFooter}
        onEndReached={() => {
          dispatch(LocationsGetAllAction(loc?.next));
        }}
        onRefresh={() => {
          dispatch(LocationsRefreshAction());
        }}
        refreshing={Boolean(loading)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.app.secondary,
  },
  flatlist: {
    backgroundColor: colors.app.secondary,
    marginHorizontal: 10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginVertical: 0,
    paddingHorizontal: 10,
  },
  itemBook: {
    marginVertical: 5,
  },
  itembutton: {marginHorizontal: 10},
  header: {flex: 1, marginVertical: 20},
  vin: {height: 100, marginTop: 10, paddingVertical: 5},
  loader: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    flex: 1,
  },
});

export default LocationsScreen;

interface CharactersScreenProps {
  route?: RouteProp<any>;

  navigation?: CompositeNavigationProp<
    StackNavigationProp<any, any>,
    CompositeNavigationProp<NavigationProp<any>, StackNavigationProp<any>>
  >;
}
