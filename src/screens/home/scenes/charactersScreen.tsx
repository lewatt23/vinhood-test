/* eslint-disable camelcase */

import {
  HomeBottomTabParamList,
  RootStackParamList,
} from '@VinSrc/navigation/constants.navigation';
import {colors} from '@VinSrc/utils';
import {
  CompositeNavigationProp,
  NavigationProp,
  RouteProp,
  useFocusEffect,
} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import React, {
  FunctionComponent,
  useState,
  useEffect,
  useCallback,
} from 'react';
import {
  ActivityIndicator,
  FlatList,
  ListRenderItem,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {VinCard} from '@VinSrc/components/card';
import {Picker} from '@react-native-picker/picker';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

import VinHearder from '@VinSrc/components/header';
import VinInput from '@VinSrc/components/input';
import {
  selectCharactersPaginationInfo,
  selectCharacters,
  selectCharactersLoading,
} from '@VinSrc/store/reducers/characters';
import {
  CharactersFilterAction,
  CharactersGetAllAction,
  CharactersRefreshAction,
  CharactersGetByLocationAction,
} from '@VinSrc/store/actions';

const CharactersScreen: FunctionComponent<CharactersScreenProps> = ({
  route,
}) => {
  const [gender, setGender] = useState<string | undefined>(undefined);
  const [status, setStatus] = useState<string | undefined>(undefined);
  const [species, setSpecies] = useState<string | undefined>(undefined);
  const [name, setName] = useState<string | undefined>(undefined);

  const dispatch = useDispatch();

  const charac: any = useSelector(selectCharactersPaginationInfo);
  const characters = useSelector(selectCharacters);
  const loading = useSelector(selectCharactersLoading);

  const getUserByLocation = async () => {
    const getLocation = await AsyncStorage.getItem('location');

    if (getLocation) {
      console.log('getLocation', getLocation);
      await AsyncStorage.removeItem('location');
      dispatch(CharactersGetByLocationAction(parseInt(getLocation, 10)));
    }
  };

  useEffect(() => {
    dispatch(CharactersGetAllAction(charac?.next));
  }, []);

  useFocusEffect(
    useCallback(() => {
      getUserByLocation();
    }, []),
  );

  useEffect(() => {
    if (species !== undefined || status !== undefined || gender !== undefined) {
      let url = 'https://rickandmortyapi.com/api/character';

      if (gender) {
        url = `https://rickandmortyapi.com/api/character/?gender=${gender}`;
      }
      if (status) {
        url = `https://rickandmortyapi.com/api/character/?status=${status}`;
      }
      if (species) {
        url = `https://rickandmortyapi.com/api/character/?species=${species}`;
      }
      if (gender && status) {
        url = `https://rickandmortyapi.com/api/character/?gender=${gender}&status=${status}`;
      }
      if (gender && species) {
        url = `https://rickandmortyapi.com/api/character/?gender=${gender}&species=${species}`;
      }
      if (species && status) {
        url = `https://rickandmortyapi.com/api/character/?status=${status}&species=${species}`;
      }
      if (species && status && gender) {
        url = `https://rickandmortyapi.com/api/character/?status=${status}&species=${species}&${gender}`;
      }

      dispatch(CharactersFilterAction(url));
    }
  }, [dispatch, gender, name, species, status]);

  const renderItem: ListRenderItem<any> = ({item}) => (
    <View style={styles.itemBook}>
      <VinCard
        onPressCard={() => {}}
        post={{
          name: item.name,
          status: item.status,
          uri: item.image,
          gender: item.gender,
          type: item.type,
          origin: item.origin.name,
        }}
      />
    </View>
  );

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.pickerContainer}>
        <Picker
          mode="dropdown"
          style={styles.picker}
          itemStyle={styles.itemPicker}
          selectedValue={status}
          onValueChange={itemValue => setStatus(itemValue)}>
          <Picker.Item label="Select a Status" value={undefined} />
          <Picker.Item label="alive" value="alive" />
          <Picker.Item label="dead" value="dead" />
          <Picker.Item label="unknown" value="unknown" />
        </Picker>
      </View>

      <View style={styles.margin}>
        <VinInput
          keyboardType="default"
          placeholder="Species"
          placeholderTextColor={colors.text.tertiaryGrey}
          backgroundColor={colors.app.white}
          borderColor={colors.app.secondaryGrey}
          onChangeText={(name, text) => {
            setSpecies(text);
          }}
        />
      </View>
      <View style={styles.pickerContainer}>
        <Picker
          mode="dropdown"
          style={styles.picker}
          itemStyle={styles.itemPicker}
          selectedValue={gender}
          onValueChange={itemValue => setGender(itemValue)}>
          <Picker.Item label="Select a Gender" value={undefined} />
          <Picker.Item label="female" value="female" />
          <Picker.Item label="male" value="male" />
          <Picker.Item label="genderless" value="genderless" />
          <Picker.Item label="unknown" value="unknown" />
        </Picker>
      </View>
    </View>
  );

  const renderEmpty = () => (
    <View>
      <Text>No Characters found!</Text>
    </View>
  );

  const renderFooter = () => {
    if (charac?.next) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color={colors.app.primary} />
        </View>
      );
    }
    return <View />;
  };

  return (
    <View style={styles.container}>
      <VinHearder
        logo
        searchBottom
        getSearchText={text => {
          dispatch(
            CharactersFilterAction(
              `https://rickandmortyapi.com/api/character/?name=${text}`,
            ),
          );
        }}
      />

      <FlatList
        style={styles.flatlist}
        initialNumToRender={5}
        keyExtractor={item => String(item.id)}
        data={characters || []}
        renderItem={renderItem}
        ListHeaderComponent={renderHeader}
        ListEmptyComponent={renderEmpty}
        numColumns={2}
        columnWrapperStyle={styles.columnStyle}
        ListFooterComponent={renderFooter}
        onEndReached={() => {
          dispatch(CharactersGetAllAction(charac?.next));
        }}
        onRefresh={() => {
          dispatch(CharactersRefreshAction());
        }}
        refreshing={Boolean(loading)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.app.secondary,
  },
  flatlist: {},
  itemBook: {
    marginVertical: 5,
  },
  itembutton: {marginHorizontal: 10},
  header: {flex: 1, marginVertical: 10, marginTop: 20, marginHorizontal: 20},
  columnStyle: {
    justifyContent: 'space-evenly',
  },
  pickerContainer: {
    borderRadius: 15,
    borderWidth: 2,
    overflow: 'hidden',
    height: 50,
    padding: 0,
    backgroundColor: colors.app.white,
    borderColor: colors.app.secondaryGrey,
  },
  picker: {
    width: '100%',
    backgroundColor: colors.app.white,
    borderColor: colors.app.black,
    borderWidth: 20,
    borderRadius: 20,
  },
  itemPicker: {
    width: 300,
    alignContent: 'center',
    textAlign: 'center',
  },
  margin: {marginVertical: 5},
  loader: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    flex: 1,
  },
});

export default CharactersScreen;

interface CharactersScreenProps {
  route?: RouteProp<any>;

  navigation?: CompositeNavigationProp<
    StackNavigationProp<any, any>,
    CompositeNavigationProp<
      NavigationProp<HomeBottomTabParamList>,
      StackNavigationProp<RootStackParamList>
    >
  >;
}
