/* eslint-disable arrow-body-style */
import React, {FunctionComponent} from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {HomeStack} from '@VinSrc/screens/home';

import {colors} from '@VinSrc/utils';

import {getFocusedRouteNameFromRoute, Route} from '@react-navigation/native';

import Icon from 'react-native-vector-icons/Ionicons';

import LocationsScreen from '@VinSrc/screens/home/scenes/locationsScreen';
import {
  HomeBottomTabParamList,
  HomeBottomTabRouteList,
} from './constants.navigation';

const BottomIconStack: {
  [index in keyof HomeBottomTabParamList]: string;
} = {
  [HomeBottomTabRouteList.CharactersStack]: 'people',
  [HomeBottomTabRouteList.LocationsStack]: 'globe',
};

const TabBottomNavigator = createBottomTabNavigator<HomeBottomTabParamList>();

export const HomeBotTab: FunctionComponent = () => {
  return (
    <TabBottomNavigator.Navigator
      screenOptions={({route}) => {
        return {
          headerShown: false,
          headerStyle: {
            backgroundColor: 'transparent',
            elevation: 0,
          },
          tabBarStyle: {
            backgroundColor: colors.app.white,
            elevation: 0,
            borderTopEndRadius: 20,
            borderTopStartRadius: 20,
            height: 40,
            paddingBottom: 2,
            borderTopWidth: 0,
          },
          tabBarItemStyle: {
            marginTop: 10,
            paddingTop: 8,
            marginBottom: 8,
          },
          tabBarLabelStyle: {fontSize: 12},
          tabBarActiveTintColor: colors.app.primary,
          tabBarIcon: ({focused}) => {
            const icon =
              BottomIconStack[route.name as keyof HomeBottomTabParamList];

            return focused ? (
              <Icon name={icon} size={15} color={colors.app.primary} />
            ) : (
              <Icon
                name={`${icon}-outline`}
                size={15}
                color={colors.app.primary}
              />
            );
          },
        };
      }}>
      <TabBottomNavigator.Screen
        name={HomeBottomTabRouteList.CharactersStack}
        component={HomeStack}
        options={({route}) => ({
          tabBarStyle: {
            backgroundColor: colors.app.white,
            elevation: 0,
            borderTopEndRadius: 20,
            borderTopStartRadius: 20,
            height: getTabBarVisibility(route) === 0 ? 0 : 60,
            paddingBottom: 2,
            borderTopWidth: 0,
            width: getTabBarVisibility(route) === 0 ? 0 : undefined,
          },
          tabBarLabel: 'Characters',
        })}
      />
      <TabBottomNavigator.Screen
        name={HomeBottomTabRouteList.LocationsStack}
        component={LocationsScreen}
        options={({route}) => ({
          tabBarLabel: 'Locations',
          tabBarStyle: {
            backgroundColor: colors.app.white,
            elevation: 0,
            borderTopEndRadius: 20,
            borderTopStartRadius: 20,
            height: getTabBarVisibility(route) === 0 ? 0 : 60,
            paddingBottom: 2,
            borderTopWidth: 0,
            width: getTabBarVisibility(route) === 0 ? 0 : undefined,
          },
        })}
      />
    </TabBottomNavigator.Navigator>
  );
};

const getTabBarVisibility = (
  route: Partial<Route<string, object | undefined>>,
) => {
  const routeName = getFocusedRouteNameFromRoute(route) ?? 'HomeTab';

  if (routeName !== 'HomeTab') {
    return 0;
  }
  return 1;
};
