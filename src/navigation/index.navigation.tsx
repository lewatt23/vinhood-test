import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

import {RootStackRouteList, RootStackParamList} from './constants.navigation';
import {HomeBotTab} from './homeBottomTab';

const StackNavigator = createStackNavigator<RootStackParamList>();

const AppNavigationContainer = () => (
  <NavigationContainer>
    <StackNavigator.Navigator initialRouteName={RootStackRouteList.BottomTab}>
      <StackNavigator.Screen
        name={RootStackRouteList.BottomTab}
        component={HomeBotTab}
        options={{
          headerShown: false,
          headerStyle: {shadowOpacity: 0, elevation: 0},
        }}
      />
    </StackNavigator.Navigator>
  </NavigationContainer>
);

export default AppNavigationContainer;
