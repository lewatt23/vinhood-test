export type NavigatorRouteList<ParamList> = {
  [index in keyof ParamList]: index;
};

export type NavigatorStackParamList<ParamList> =
  | {
      screen: keyof ParamList;
      params: ParamList[keyof ParamList];
    }
  | undefined;

export type RootStackParamList = {
  BottomTab: undefined;
};

export type HomeBottomTabParamList = {
  CharactersStack: NavigatorStackParamList<any>;
  LocationsStack: NavigatorStackParamList<any>;
};

export const RootStackRouteList: NavigatorRouteList<RootStackParamList> = {
  BottomTab: 'BottomTab',
};

export const HomeBottomTabRouteList: NavigatorRouteList<HomeBottomTabParamList> =
  {
    CharactersStack: 'CharactersStack',
    LocationsStack: 'LocationsStack',
  };
