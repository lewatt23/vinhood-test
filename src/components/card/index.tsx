import React, {FunctionComponent} from 'react';
import {View, StyleSheet, Text, ViewStyle, Image} from 'react-native';
import {colors} from '@VinSrc/utils';

import {TouchableOpacity} from 'react-native-gesture-handler';

interface VinCardInterface {
  onPressCard: () => void;
  onPressRefresh?: () => void;

  post: {
    uri: string;
    name: string;
    status: string;
    gender: string;
    type: string;
    origin: string;
  };
  style?: ViewStyle;
}

export const VinCard: FunctionComponent<VinCardInterface> = ({
  post,
  style,
  onPressCard,
}) => (
  // const navigation = useNavigation();

  <View style={[styles.card, style]}>
    <View style={styles.cardCover}>
      <TouchableOpacity style={styles.imageContent} onPress={onPressCard}>
        <Image
          style={[styles.cardImage]}
          source={{
            uri: post.uri,
          }}
          resizeMode="cover"
        />
      </TouchableOpacity>

      <View style={[styles.headerCard]}>
        <View style={[styles.head]}>
          <View>
            <Text style={[styles.cardTitle]}>{post.name}</Text>
            <View style={styles.rowIn}>
              <View
                style={{
                  width: 10,
                  height: 10,
                  borderRadius: 10,
                  marginRight: 10,
                  backgroundColor: post.status === 'Alive' ? 'green' : 'red',
                }}
              />

              <View style={[styles.cardText]}>
                <Text>
                  {post.status}-{post.type}
                </Text>
              </View>
            </View>
            <Text style={styles.earth}>{post.origin}</Text>
          </View>
        </View>
      </View>
      <View style={styles.flex}>
        <Text style={styles.textBottom}>Gender:</Text>
        <Text
          style={[
            styles.textBottom,
            {
              fontWeight: '800',
            },
          ]}>
          Male
        </Text>
      </View>
      <View style={styles.flex}>
        <Text style={styles.textBottom}>Origin:</Text>
        <Text
          style={[
            styles.textBottom,
            {
              fontSize: 12,

              fontWeight: '800',
            },
          ]}>
          Male
        </Text>
      </View>
    </View>
  </View>
);

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  flex: {flexDirection: 'row', justifyContent: 'space-between'},
  card: {
    backgroundColor: colors.app.white,

    borderRadius: 5,

    marginTop: 5,
    marginBottom: 0,
    padding: 5,
    minWidth: 180,
  },
  headerCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 15,
  },
  category: {paddingHorizontal: 5, fontSize: 12},
  cardText: {
    fontSize: 14,
    marginVertical: 2,
    fontWeight: '400',

    lineHeight: 20,
    color: colors.text.black,
  },
  cardTitle: {
    fontSize: 16,
    marginVertical: 2,
    marginTop: 7,
    fontWeight: '700',

    lineHeight: 20,
    color: colors.text.black,
  },
  cardContent: {
    paddingTop: 0,
  },
  cardCover: {paddingHorizontal: 20},

  imageContent: {
    marginTop: 2,
  },
  cardImage: {
    // height: 200,
    minHeight: 100,
    width: '100%',
    borderRadius: 5,
    marginBottom: 10,
  },
  earth: {
    fontSize: 14,
    marginVertical: 2,
    marginTop: 7,
    fontWeight: '300',

    lineHeight: 20,
    color: colors.text.black,
  },
  cardFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  actionIcons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  number: {
    fontSize: 11,
    color: colors.app.black,
    position: 'relative',
    left: -10,
  },
  mark: {
    position: 'relative',
    right: 0,
  },
  more: {
    position: 'relative',
    right: -12,
  },
  markIcon: {
    height: 14,
    width: 14,
  },
  textBottom: {
    fontSize: 14,

    fontWeight: '300',

    color: colors.text.black,
  },
  marginLeft: {
    marginLeft: 10,
  },
  timeSmall: {
    fontSize: 10,
    color: colors.app.purpleLight,
  },
  nameSmall: {
    fontSize: 12,

    color: colors.app.black,
    fontWeight: 'bold',
  },
  horizontalStroke: {
    height: 1,
    backgroundColor: colors.app.borderColor,
    marginTop: 10,
    marginBottom: 20,
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  marginBottomSmall: {
    marginBottom: 20,
  },

  seeMore: {
    paddingHorizontal: 15,
    color: colors.text.blue,
  },
  videoPoster: {
    top: 0,
    minHeight: 200,
    width: '100%',
    borderRadius: 14,
  },
  rowIn: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  play: {
    position: 'absolute',
    marginVertical: 80,
    alignSelf: 'center',
  },
  dangar: {
    color: colors.text.danger,
  },
  dateSponsored: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  sponsored: {
    fontWeight: '500',
    fontSize: 12,
    color: colors.text.blue,
  },
  head: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
