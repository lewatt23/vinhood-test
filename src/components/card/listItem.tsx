import {colors} from '@VinSrc/utils';
import React, {FunctionComponent} from 'react';
import {
  ViewStyle,
  TouchableOpacity,
  View,
  StyleSheet,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const VinListItemSimple: FunctionComponent<ListItemProps> = ({
  title,
  type = '',
  dimension = '',
  number,
  ...props
}) => (
  <TouchableOpacity
    onPress={props.onPress}
    style={[styles.container, props.style]}>
    <View style={styles.mainContent}>
      <Icon name="md-location-outline" size={30} color={colors.app.black} />
      <View style={styles.titleDescription}>
        <View style={styles.containerTitle}>
          <Text style={styles.title}>{title}</Text>
        </View>
        <View style={styles.containerTitle}>
          <Text style={[styles.title, {fontSize: 12}]}>Type : {type}</Text>
        </View>
        <View style={styles.containerTitle}>
          <Text style={[styles.title, {fontSize: 12}]}>
            Dimension : {dimension}
          </Text>
        </View>
        <View style={styles.containerTitle}>
          <Text style={[styles.title, {fontSize: 12}]}>
            Number of residents : {number}
          </Text>
        </View>
      </View>
    </View>
    <Icon name="ios-chevron-forward" size={20} color={colors.app.black} />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    borderColor: colors.app.white,
    borderRadius: 10,
    backgroundColor: colors.app.white,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 3,
    alignItems: 'center',
    alignContent: 'center',
    // alignSelf: 'center'
  },
  mainContent: {
    flexDirection: 'row',
    flexShrink: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleDescription: {
    flexShrink: 1, // fixes overflow on text exceeding view
  },
  bookImage: {height: 40, width: 40, marginRight: 10},
  title: {
    fontWeight: '400',
    fontSize: 16,
    color: colors.text.black,
    alignItems: 'center',
  },
  containerTitle: {marginLeft: 10},
  authorContainer: {flexDirection: 'row'},
  author: {fontSize: 12, marginVertical: 5, color: colors.text.lightGrey},
  priceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  price: {
    fontWeight: 'bold',
    fontSize: 20,
    color: colors.app.primary,
  },
  pricePadding: {padding: 2},
  ava: {color: colors.text.black, margin: 2, paddingHorizontal: 5},
  clock: {flexDirection: 'row'},
  time: {
    fontSize: 12,
    color: colors.app.primary,
    alignSelf: 'center',
  },
});

interface ListItemProps {
  title?: string;
  style?: ViewStyle;
  type: string;
  dimension: string;
  number: number;
  onPress?: () => any;
}

export {VinListItemSimple};
