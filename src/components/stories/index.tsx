import { EnumMediaType } from '@PrSrc/globalTypes';
import { StoryGetMany_storyGetMany } from '@PrSrc/screens/home/graphql/__generated__/StoryGetMany';
import { colors, formateDate } from '@PrSrc/utils';
import React, { FunctionComponent } from 'react';
import { StyleSheet } from 'react-native';
import Stories from 'react-native-media-stories';
import { StoriesType } from 'react-native-media-stories/src';

interface IPrenStories {
  data: ((StoryGetMany_storyGetMany | null)[] | null)[] | undefined | null;
  // data: StoriesType[];
  sendMessage?: Element;
  onPressAddIcon?: () => void;
  onPressComment?: () => void;
}

const structureStoriesData = (
  data: ((StoryGetMany_storyGetMany | null)[] | null)[] | undefined | null,
): StoriesType[] => {
  const stories: StoriesType[] = [];

  if (data && data!.length > 0) {
    data!.forEach((story) => {
      const temp: StoriesType = {
        username: story![0]!.author!.user!.username,
        profile: story![0]!.author!.profilePicMedia!.url,
        isCertified: false,
        live: story![0]!.author!.user!.isOnline!,
        viewed: false,
        userId: story![0]!.createdById,
        stories: [],
      };

      story?.forEach((el) => {
        temp.stories.push({
          id: el?._id!,
          url:
            el?.media?.type === EnumMediaType.video
              ? el?.media.hlsStreamUrl
              : el?.media?.url!,
          type: el?.storyType!,
          duration: 4,
          isSeen: true,
          isPaused: true,
          created: formateDate({
            date: String(el!.createdAt),
            format: 'L',
            type: 'FROMNOW',
          }),
          storyid: el?._id,
          title: '',
        });
      });

      stories.push(temp);
    });
  }

  return stories;
};

const PrStories: FunctionComponent<IPrenStories> = (props: IPrenStories) => {
  const stories = structureStoriesData(props.data);

  return (
    <Stories
      data={stories}
      readMoreIcon={props.sendMessage}
      onPressAddIcon={props.onPressAddIcon ? props.onPressAddIcon : () => {}}
      onReadMore={props.onPressComment ? props.onPressComment : () => {}}
      liveStyle={styles.live}
      viewedStyle={styles.viewed}
      boxAvatarStyle={styles.boxAvatar}
    />
  );
};
export default PrStories;

const styles = StyleSheet.create({
  live: {
    backgroundColor: colors.app.danger,
  },
  boxAvatar: {
    borderColor: colors.app.primary,
  },
  viewed: {
    borderColor: colors.app.tertiary,
  },
});
