import { colors } from '@PrSrc/utils';
import React, { FunctionComponent, useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import {
  StoryGetMany,
  StoryGetMany_storyGetMany,
} from '@PrSrc/screens/home/graphql/__generated__/StoryGetMany';
import { useQuery } from '@apollo/client';
import { QUERY_STORY_GET_MANY } from '@PrSrc/screens/home/graphql/queries';
import {
  ImageLibraryOptions,
  ImagePickerResponse,
  launchImageLibrary,
} from 'react-native-image-picker';

import {
  HomeStackParamList,
  HomeStackRouteList,
} from '@PrSrc/screens/home/constants';
import { CompositeNavigationProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import i18n from '@PrSrc/config/i18n/i18n';
import { RootStackRouteList } from '@PrSrc/navigation/constants.navigation';
import { useSelector } from 'react-redux';
import { selectAuth } from '@PrSrc/store/reducers/users';
import PrStories from './index';
import { PrButton } from '../button';
import PrDefaultInput from '../defaultInput';
import PrIcon from '../Icon';
import PrAlert from '../alert';

const options: ImageLibraryOptions = {
  mediaType: 'mixed',
};

export const PrStoriesComponent: FunctionComponent<StoriesComponent> = ({
  navigation,
}) => {
  const [loading, setLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const auth = useSelector(selectAuth);

  // Image picker

  const handleImagePicker = async () => {
    setLoading(true);

    launchImageLibrary(options, (res: ImagePickerResponse) => {
      if (res.assets) {
        if (res.assets.length > 0 && !loading) {
          navigation.navigate(HomeStackRouteList.CreateStoriesScreen, {
            file: res.assets![0],
          });
        }
      } else if (res.errorCode === 'permission') {
        // Ask for permission
      }
      setLoading(false);
    });
  };

  const queryStoryGetMeny = useQuery<StoryGetMany>(QUERY_STORY_GET_MANY, {});

  const [storyGetMany, setSotryGetMany] = useState<
    ((StoryGetMany_storyGetMany | null)[] | null)[] | undefined | null
  >(queryStoryGetMeny.data?.storyGetMany);

  useEffect(() => {
    setSotryGetMany(queryStoryGetMeny?.data?.storyGetMany);
  }, [queryStoryGetMeny.data]);

  useEffect(() => {
    queryStoryGetMeny.refetch();
  }, [navigation, queryStoryGetMeny]);

  return (
    <View style={storiesStyle.stories}>
      <PrStories
        data={storyGetMany}
        onPressComment={() => {}}
        onPressAddIcon={
          auth.user ? () => handleImagePicker() : () => setShowAlert(true)
        }
        sendMessage={
          <View style={storiesStyle.inputBox}>
            <PrDefaultInput
              editable={false}
              borderColor={colors.app.white}
              placeholder="Send a message"
              backgroundColor={colors.app.backgroundStories}
              styles={storiesStyle.textfield}
            />
            <PrButton
              disabled
              icon={
                <PrIcon
                  name="telegram"
                  fill="none"
                  stroke={colors.app.white}
                  width="40"
                  height="40"
                  viewBox="0 -7 15 35"
                />
              }
              size="lg"
              color={colors.app.primary}
              tintColor={colors.app.white}
            />
          </View>
        }
      />
      <PrAlert
        showAlert={showAlert}
        title={i18n.t('COMMON__ALERT_LOGIN_TITLE')}
        message={i18n.t('COMMON__ALERT_LOGIN_MESSAGE')}
        confirmText={i18n.t('COMMON__ALERT_LOGIN_CONFIRM')}
        cancelText={i18n.t('COMMON__ALERT_LOGIN_CANCEL')}
        onComfirmPress={() => {
          navigation.navigate(RootStackRouteList.Login);
        }}
        onCancelPress={() => {}}
        onDismiss={() => {
          setShowAlert(false);
        }}
      />
    </View>
  );
};

const storiesStyle = StyleSheet.create({
  stories: {
    marginTop: 10,
  },
  inputBox: {
    flexDirection: 'row',
    paddingLeft: 30,
    paddingRight: 30,
    marginBottom: 20,
    alignItems: 'center',
  },
  textfield: {
    flex: 1,
    marginRight: 10,
  },
});

interface StoriesComponent {
  navigation: CompositeNavigationProp<
    StackNavigationProp<HomeStackParamList, typeof HomeStackRouteList.HomeTab>,
    CompositeNavigationProp<any, any>
  >;
}
