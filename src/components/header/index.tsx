import {colors} from '@VinSrc/utils';
import images from '@VinSrc/utils/images';
import Icon from 'react-native-vector-icons/Ionicons';
// import {useNavigation} from '@react-navigation/native';
import React, {FunctionComponent, useState} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

import VinInput from '../input';

interface Props {
  title?: string;
  logo?: boolean;
  transparent?: boolean;

  previous?: boolean;

  searchBottom?: boolean;

  onPressBack?: () => void;
  onPressSearch?: () => void;

  getSearchText?: (text: string) => void;
}

const VinHearder: FunctionComponent<Props> = (props: Props) => {
  // const navigation = useNavigation<any>();

  const [search, setSearch] = useState<string>();
  return (
    <View
      style={[
        styles.containerParent,
        props.transparent && styles.backgroundTransparent,
      ]}>
      <View style={[styles.container]}>
        {props.logo && <Image style={styles.logo} source={images.logoSmall} />}

        {props.title && (
          <View style={{flexGrow: 1, width: 100}}>
            <Text style={styles.title}>{props.title.slice(0, 30)}</Text>
          </View>
        )}
      </View>
      {props.searchBottom && (
        <View style={styles.searchBottom}>
          <VinInput
            prependIcon={
              <Icon name="search-outline" size={20} color={colors.app.black} />
            }
            keyboardType="default"
            placeholder="Search a topic, category…"
            placeholderTextColor={colors.text.tertiaryGrey}
            borderColor={colors.app.white}
            backgroundColor={colors.app.white}
            value={search}
            onChangeText={(name, text) => {
              setSearch(text);
              if (props.getSearchText) props.getSearchText(text);
            }}
          />
        </View>
      )}
    </View>
  );
};
export default VinHearder;

const styles = StyleSheet.create({
  containerParent: {
    backgroundColor: colors.app.primary,
    paddingHorizontal: 12,
    paddingVertical: 10,
    borderBottomEndRadius: 10,
    borderBottomStartRadius: 10,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  alignItemRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backgroundTransparent: {
    backgroundColor: 'transparent',
  },
  logo: {
    width: 100,
    height: 50,
    resizeMode: 'contain',
  },
  title: {
    // alignSelf: 'center',
    fontWeight: 'bold',

    color: colors.text.white,
    textTransform: 'capitalize',
  },
  lead: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexGrow: 1,
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  marginLeft: {
    marginLeft: 15,
  },
  userStyle: {
    marginLeft: 10,
    fontWeight: 'bold',

    color: colors.app.white,
    textTransform: 'capitalize',
  },
  userOnlineStyle: {
    marginLeft: 10,
    fontWeight: '100',

    color: colors.app.white,
    textTransform: 'capitalize',
    marginTop: 2,
  },
  userOnline: {
    width: 10,
    height: 10,
    borderWidth: 1,
    borderColor: colors.app.white,
    backgroundColor: colors.app.blue,
    left: 45,
  },
  avatarChat: {
    marginLeft: 15,
    height: 45,
    width: 45,
  },
  button: {
    paddingHorizontal: 15,
    backgroundColor: colors.app.primary,
    borderRadius: 8,
  },
  textButton: {
    fontSize: 14,
  },
  notificationCount: {
    color: colors.text.white,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 12,
  },
  notification: {
    backgroundColor: colors.app.primary,
    width: 18,
    height: 18,
    position: 'absolute',
    zIndex: 99999,
    borderRadius: 10,
    left: 30,
    top: -3,
  },
  searchCustom: {justifyContent: 'center', flex: 1},
  searchBottom: {justifyContent: 'center', marginTop: 10},
  clockButton: {
    justifyContent: 'center',
    padding: 1,
    paddingHorizontal: 2,
    marginLeft: 3,
  },
  userButton: {
    justifyContent: 'center',
    padding: 1,
    paddingHorizontal: 2,
    marginRight: 3,
  },
  clockContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingHorizontal: 2,
  },
  clockText: {
    color: colors.text.white,
    fontSize: 15,
  },
  progressContainer: {alignSelf: 'center', flex: 1},
});
