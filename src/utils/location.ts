const getCharacterIDFromUrl = (data: string[]): string[] => {
  const IDS: string[] = [];
  data.forEach(url => {
    IDS.push(url.substring(url.lastIndexOf('/') + 1));
  });

  return IDS;
};

export {getCharacterIDFromUrl};
