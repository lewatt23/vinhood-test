import formateDate from 'src/utils/date';
import {colors} from './colors';

import images from './images';

export {colors, formateDate, images};
