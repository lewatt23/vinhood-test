import {DateFormat, Dates} from '@VinSrc/typings';

import moment from 'moment';
import 'moment/min/locales';

moment.locale('en');
const formateDate: DateFormat = (dateMoment: Dates) => {
  switch (dateMoment.type) {
    case 'FROMNOW':
      return moment(dateMoment.date).fromNow();
    default:
      return moment(dateMoment.date).format(dateMoment.format);
  }
};

export default formateDate;
