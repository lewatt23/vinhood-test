/**
 * Application Based Colors
 */

export const colors = {
  app: {
    primary: '#595DE5',
    secondary: '#F1F3FC',
    tertiary: '#DCDEFE',
    grey: '#F8FAFF',
    secondaryGrey: '#E9F0FB',
    tertiaryGrey: '#899198',
    quaternaryGrey: '#979797',
    purpleLight: '#959ABF',
    white: '#FDFEFF',
    black: '#000000',
    success: '#34D84E',
    blue: '#108ED6',
    danger: '#ED1C24',
    borderColor: '#E8E8F1',
    quinaryGrey: '#E0E3EF',
    lightGrey: '#9B9B9B',
    backgroundStories: 'rgba(255, 255, 255, 0.7)',
  },
  text: {
    primary: '#1F1F1F',
    purple: '#595DE5',
    secondary: '#B4B4B4',
    purpleLight: '#959ABF',
    grey: '#F8FAFF',
    secondaryGrey: '#F1F3FC',
    tertiaryGrey: '#899198',
    white: '#FDFEFF',
    black: '#000000',
    success: '#34D84E',
    blue: '#108ED6',
    danger: '#ED1C24',
    lightGrey: '#9B9B9B',
  },
};
