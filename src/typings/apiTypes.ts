export interface ICharacter {
  id: number;
  name: string;
  status: string;
  species: string;
  type: string;
  gender: string;
  origin: object;
  location: object;
  image: string;
  url: [string];
  created: string;
}

export interface ILocation {
  id: number;
  name: string;
  type: string;
  dimension: string;
  residents: [string];
  url: string;
  created: string;
}

export interface IPagintionInfo {
  count: number;
  pages: number;
  next?: string;
  prev?: string;
}
