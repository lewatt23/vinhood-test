module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        cwd: 'babelrc',
        extensions: ['.ts', '.tsx', '.js', '.ios.js', '.android.js'],
        root: ['.'],

        alias: {
          '@VinSrc': './src',
          '@VinStorybook': './storybook',
          '@VinComponents': './src/components',
          '@VinAssets': './src/assets',
          '@VinConfig': './src/config',
          '@VinNavigation': './src/navigation',
          '@VinScreen': './src/screens',
          '@VinUtils': './src/utils',
          '@VinTypings': './src/typings',
          '@VinServices': 'src/services',
          '@VinStore': 'src/store',
        },
      },
    ],
    // 'jest-hoist',
  ],
};
