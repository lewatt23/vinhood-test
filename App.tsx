import {persistor, store} from '@VinSrc/config';
import React from 'react';
import {StatusBar} from 'react-native';
import {colors} from '@VinSrc/utils';
import FlashMessage from 'react-native-flash-message';
import AppNavigationContainer from '@VinSrc/navigation/index.navigation';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import InterceptorProvider from '@VinSrc/config/interceptorProvider';

const App = () => (
  <SafeAreaProvider>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <InterceptorProvider store={store}>
          <>
            <StatusBar
              barStyle="light-content"
              backgroundColor={colors.app.primary}
            />
            <AppNavigationContainer />
            <FlashMessage position="top" />
          </>
        </InterceptorProvider>
      </PersistGate>
    </Provider>
  </SafeAreaProvider>
);

export default App;
