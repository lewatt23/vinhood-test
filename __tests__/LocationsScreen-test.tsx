/* eslint-disable import/order */
import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import LocationsScreen from '@VinSrc/screens/home/scenes/locationsScreen';

it('Location  screen  renders correctly', () => {
  const tree = renderer.create(<LocationsScreen />).toJSON();
  expect(tree).toMatchSnapshot();
});
