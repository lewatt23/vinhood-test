/* eslint-disable import/order */
import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import CharactersScreen from '@VinSrc/screens/home/scenes/charactersScreen';

it('Characters  screen  renders correctly', () => {
  const tree = renderer.create(<CharactersScreen />).toJSON();
  expect(tree).toMatchSnapshot();
});
